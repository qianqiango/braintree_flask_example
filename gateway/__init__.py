import os
from dotenv import load_dotenv
import braintree

gateway = braintree.BraintreeGateway(
    braintree.Configuration(
        environment=os.environ.get('BT_ENVIRONMENT'),
        merchant_id=os.environ.get('BT_MERCHANT_ID'),
        public_key=os.environ.get('BT_PUBLIC_KEY'),
        private_key=os.environ.get('BT_PRIVATE_KEY')
    )
)


def generate_client_token():
    return gateway.client_token.generate()
# end def


def vault_payment_method(payment_method_nonce):
    result = gateway.customer.create({})

    if result.is_success:
        customer_id = result.customer.id
        return gateway.payment_method.create({
            "customer_id": customer_id,
            "payment_method_nonce": payment_method_nonce
        })
    else:
        return result
    # end if
# end def


def transact(options):
    return gateway.transaction.sale(options)
# end def


def find_transaction(id):
    return gateway.transaction.find(id)
# end def
