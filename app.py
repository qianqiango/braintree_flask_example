from flask import Flask, redirect, url_for, render_template, request, flash

import os
from os.path import join, dirname
import uuid
import sqlalchemy
import json
from dotenv import load_dotenv
import braintree
from gateway import generate_client_token, transact, find_transaction, vault_payment_method

load_dotenv()

app = Flask(__name__)
app.secret_key = os.environ.get('APP_SECRET_KEY')

PORT = int(os.environ.get('PORT', 4567))

TRANSACTION_SUCCESS_STATUSES = [
    braintree.Transaction.Status.Authorized,
    braintree.Transaction.Status.Authorizing,
    braintree.Transaction.Status.Settled,
    braintree.Transaction.Status.SettlementConfirmed,
    braintree.Transaction.Status.SettlementPending,
    braintree.Transaction.Status.Settling,
    braintree.Transaction.Status.SubmittedForSettlement
]

engine = sqlalchemy.create_engine(os.environ.get('DATABASE_URI'))


@app.route('/', methods=['GET'])
def index():
    return redirect(url_for('new_checkout'))
# end def


@app.route('/auth/generation/<source>', methods=['POST'])
def generate_auth_link(source):
    params = request.get_json()
    print(params)

    # get success url and fail url
    success_url = params['success_url'] if 'success_url' in params else None
    fail_url = params['fail_url'] if 'fail_url' in params else None
    if (success_url is None) or (fail_url is None):
        return "Missing required parameters"
    # end if

    # generage unique transaction Id
    transaction_id = uuid.uuid1()
    URL = "http://192.168.1.109:4567/auth/{}".format(transaction_id,)

    # save transaction
    engine.execute("INSERT INTO transactions (transaction_id, source, url, success_redirect, fail_redirect) VALUES ('{}', '{}', '{}', '{}', '{}')".format(
        transaction_id, source, URL, success_url, fail_url))

    return URL
# end def


@app.route('/auth/<transactionId>', methods=['GET'])
def auth(transactionId):
    client_token = generate_client_token()
    return render_template('checkouts/auth.html', client_token=client_token, transactionId=transactionId)
# end def


@app.route('/auth/submit', methods=['POST'])
def submit_auth():

    print(request.form['payment_method_nonce'])
    print(request.form['transaction_id'])

    transaction_id = request.form['transaction_id']

    # get transaction by transaction_id
    query = engine.execute(
        "SELECT success_redirect, fail_redirect FROM transactions WHERE transaction_id='{}'".format(transaction_id,))
    transactions = query.fetchall()
    if len(transactions) <= 0:
        return "ERROR: invalid token"
    # end if
    print(transactions)
    success_url = transactions[0]['success_redirect']
    fail_url = transactions[0]['fail_redirect']

    # use payment_method_nonce to make payment
    result = vault_payment_method(request.form['payment_method_nonce'])

    if result.is_success or result.payment_method:
        # braintree_transaction_id = result.transaction.id
        payment_method_token = result.payment_method.token

        # update transactions table by transaction_id
        engine.execute("UPDATE transactions SET payment_method_nonce='{}', payment_result='{}', payment_method_token='{}', braintree_transaction_id='{}' WHERE transaction_id='{}'".format(
            request.form['payment_method_nonce'], "", payment_method_token, "", transaction_id))

        # test redirect
        return redirect('{}?transaction_id={}'.format(success_url, transaction_id))
    else:
        # update transactions table by transaction_id
        engine.execute("UPDATE transactions SET payment_method_nonce='{}', payment_result='{}', WHERE transaction_id='{}'".format(
            request.form['payment_method_nonce'], result.message, transaction_id))

        for x in result.errors.deep_errors:
            flash('Error: %s: %s' % (x.code, x.message))

        # test redirect
        return redirect('{}?transaction_id={}'.format(fail_url, transaction_id))
    # end if
# end def


@app.route('/charge/<payment_method_token>', methods=['POST'])
def charge(payment_method_token):
    params = request.get_json()
    print(params)

    # get success url and fail url
    amount = params['amount'] if 'amount' in params else None
    if amount is None:
        return "ERROR: invalid parameters"
    # end if

    result = transact({
        'amount': amount,
        'payment_method_token': payment_method_token
    })

    if result.is_success or result.transaction:
        return redirect(url_for('show_checkout', transaction_id=result.transaction.id))
    else:
        for x in result.errors.deep_errors:
            flash('Error: %s: %s' % (x.code, x.message))
        return "Failed to charge, ERROR: %s" % result.message
    # end if
# end def


@app.route('/checkouts/new', methods=['GET'])
def new_checkout():
    client_token = generate_client_token()
    return render_template('checkouts/new.html', client_token=client_token)
# end def


@app.route('/checkouts/<transaction_id>', methods=['GET'])
def show_checkout(transaction_id):
    transaction = find_transaction(transaction_id)
    result = {}
    if transaction.status in TRANSACTION_SUCCESS_STATUSES:
        result = {
            'header': 'Sweet Success!',
            'icon': 'success',
            'message': 'Your test transaction has been successfully processed. See the Braintree API response and try again.'
        }
    else:
        result = {
            'header': 'Transaction Failed',
            'icon': 'fail',
            'message': 'Your test transaction has a status of ' + transaction.status + '. See the Braintree API response and try again.'
        }
    # end if

    return render_template('checkouts/show.html', transaction=transaction, result=result)
# end def


@app.route('/checkouts', methods=['POST'])
def create_checkout():
    result = transact({
        'amount': request.form['amount'],
        'payment_method_nonce': request.form['payment_method_nonce'],
        'options': {
            "submit_for_settlement": True
        }
    })

    if result.is_success or result.transaction:
        return redirect(url_for('show_checkout', transaction_id=result.transaction.id))
    else:
        for x in result.errors.deep_errors:
            flash('Error: %s: %s' % (x.code, x.message))
        return redirect(url_for('new_checkout'))
    # end if
# end def


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=PORT, debug=True)
